# Importación de módulos y funciones necesarias
from abc import abstractmethod
from ast import Set
import asyncio
from functools import partial
import json
import time
from typing_extensions import deprecated
from langchain.pydantic_v1 import Field
from typing import Any, List, Mapping, Sequence, Union, Optional, Dict, Tuple, cast
from langchain_core.callbacks.manager import CallbackManagerForLLMRun
from langchain_core.language_models.llms import LLM
from dotenv import dotenv_values
import requests
from langchain_core.outputs.generation import Generation
from langchain.chat_models.base import BaseChatModel
from langchain.schema.language_model import LanguageModelInput
from langchain.schema.runnable import RunnableConfig
from langchain.schema.messages import (
    AIMessage,
    BaseMessage,
    BaseMessageChunk,
    HumanMessage,
)
from langchain_core.messages.base import BaseMessage, BaseMessageChunk
from langchain_core.outputs.chat_generation import ChatGenerationChunk, ChatGeneration
from langchain_core.outputs.chat_result import ChatResult
from langchain_core._api import beta, deprecated
from langchain.callbacks.manager import (
    AsyncCallbackManagerForLLMRun,
    CallbackManagerForLLMRun,
    Callbacks
)
from langchain_core.messages.ai import AIMessage
from functools import lru_cache

dotEnvDict = dotenv_values(".env")
API_KEY = dotEnvDict.get("LLAMUS_API_KEY")
LLAMUS_BASE_PATH = dotEnvDict.get("LLAMUS_BASE_PATH")


@lru_cache(maxsize=None)  # Cache the tokenizer
def get_tokenizer() -> Any:
    try:
        from transformers import GPT2TokenizerFast  # type: ignore[import]
    except ImportError:
        raise ImportError(
            "Could not import transformers python package. "
            "This is needed in order to calculate get_token_ids. "
            "Please install it with `pip install transformers`."
        )
    # create a GPT-2 tokenizer instance
    return GPT2TokenizerFast.from_pretrained("gpt2")


def _get_token_ids_default_method(text: str) -> List[int]:
    """Encode the text into token IDs."""
    # get the cached tokenizer
    tokenizer = get_tokenizer()

    # tokenize the text using the GPT-2 tokenizer
    return tokenizer.encode(text)

# Función auxiliar para obtener la representación curl de una respuesta HTTP
def get_curl(response):
    import curl
    curl_text = curl.parse(response, return_it=True, print_it=False)
    return curl_text

# Definición de la clase Llamus, que hereda de LLM
class Llamus(LLM):
    n: int
    api_key: str
    endpoint: str
    model: str

    @property
    def _llm_type(self) -> str:
        return "llamus"

    # Método para obtener una lista de modelos disponibles
    def list_models(self):
        headers = {
            'Content-Type': 'application/json',
            'Authorization': f"Bearer {self.api_key}"
        }
        response = requests.get(f"{self.endpoint}/api/tags", headers=headers)
        #print(response.json())
        return [model["model"] for model in response.json()["models"]]

    # Método para realizar la llamada al modelo de lenguaje subyacente
    def _call(
            self,
            prompt: str,
            messages: List[BaseMessage]=[],
            temperature: float = 0.1,
            stop: Optional[List[str]] = None,
            timeout=15,
            run_manager: Optional[CallbackManagerForLLMRun] = None,
            **kwargs: Any,
        ) -> str:
        import urllib3, socket
        from urllib3.connection import HTTPConnection
    
        HTTPConnection.default_socket_options = ( 
            HTTPConnection.default_socket_options + [
            (socket.SOL_SOCKET, socket.SO_SNDBUF, 1000000), #1MB in byte
            (socket.SOL_SOCKET, socket.SO_RCVBUF, 1000000)
        ])
        if stop is not None:
            stop = None
            #raise ValueError("stop kwargs are not permitted.")
        if temperature < 0 or temperature > 1:
            raise ValueError("Temperature should be between 0 and 1")
        headers = {
            'Content-Type': 'application/json',
            'Authorization': f"Bearer {self.api_key}"
        }
        if prompt != "":
            messages.insert(0, {
                "role": "system",
                "content": prompt
            })
        message_unique_set = set(map(lambda x: tuple(x.items()),messages))
        messages_unique = []
        for dict_items in message_unique_set:
            temp = {}
            for (k,v) in dict_items:
                temp[k] = v
            messages_unique.append(temp)

        data = {
            'model': self.model,
            'messages': messages_unique,

            'temperature': temperature,
            'stream': False,
            'max_tokens': None,
            'top_k': None,
            'top_p': None
        }
        retries = 0
        max_retries = self.n
        while retries < max_retries:
            try:
                response = requests.post(f"{self.endpoint}/v1/chat/completions", headers=headers, data=json.dumps(data), stream=False,
                                         timeout=timeout)
                response.raise_for_status()
                #curl_text = get_curl(response)
                total_response = response.json()
                return total_response["choices"][0]["message"]["content"]
            except requests.exceptions.RequestException as e:
                print(f"Request failed: {e}")
                retries += 1
                wait_time = 2 ** retries
                print(f"Retrying in {wait_time} seconds...")
                time.sleep(wait_time)
        raise Exception("Max retries exceeded, request failed.")
    
    @property
    def _identifying_params(self) -> Mapping[str, Any]:
        """Obtener los parámetros de identificación."""
        return {"n": self.n, "api_key": self.api_key, "endpoint": self.endpoint,
                "model": self.model}



# Definición de la clase LlamusChat, que hereda de BaseChatModel
class LlamusChat(BaseChatModel):
    

    @property
    def lc_serializable(self) -> bool:
        return True

    client: Any = None #: :meta private:
    api_key: str = None
    base_path: str = None
    temperature: float = 0.7
    model_name:str = Field(default="llama2:13b-chat", alias="model")
    """Nombre del modelo a utilizar."""
    model_kwargs: Dict[str, Any] = Field(default_factory=dict)
    customchat_api_key: Optional[str] = None
    request_timeout: Optional[Union[float, Tuple[float, float]]] = None
    max_retries: int = 6
    streaming: bool = False
    max_tokens: Optional[int] = None

    # Método para generar una respuesta basada en mensajes de entrada
    def _generate(
        self,
        messages: List[BaseMessage],
        stop: Optional[List[str]] = None,
        run_manager: Optional[CallbackManagerForLLMRun] = None,
        **kwargs: Any,
    ) -> ChatResult:
        output_str = self._call(messages, stop=stop, run_manager=run_manager, **kwargs)
        message = AIMessage(content=output_str)
        generation = ChatGeneration(message=message)
        return ChatResult(generations=[generation])
    
    # Método para establecer la clave API
    def set_api_key(self, api_key):
        self.api_key = api_key

    # Método para llamar al modelo subyacente
    def _call(
        self,
        messages: List[BaseMessage],
        stop: Optional[List[str]] = None,
        run_manager: Optional[CallbackManagerForLLMRun] = None,
        **kwargs: Any,
    ) -> str:
        llm = Llamus(
            n=self.max_retries,
            api_key=self.api_key,
            endpoint=self.base_path,
            model=self.model_name
        )
        prompt = " ".join([message.content for message in messages])
        return llm._call(prompt, stop=stop, **kwargs)

    # Método asincrónico para generar una respuesta
    async def _agenerate(
        self,
        messages: List[BaseMessage],
        stop: Optional[List[str]] = None,
        run_manager: Optional[AsyncCallbackManagerForLLMRun] = None,
        **kwargs: Any,
    ) -> ChatResult:
        func = partial(
            self._generate, messages, stop=stop, run_manager=run_manager, **kwargs
        )
        return await asyncio.get_event_loop().run_in_executor(None, func)

    def predict(
        self, text: str, *, stop: Optional[Sequence[str]] = None, **kwargs: Any
    ) -> str:
        if stop is None:
            _stop = None
        else:
            _stop = list(stop)
        result = self([HumanMessage(content=text)], stop=_stop, **kwargs)
        return result.content
    
    def __call__(
        self,
        messages: List[BaseMessage],
        stop: Optional[List[str]] = None,
        callbacks: Callbacks = None,
        **kwargs: Any,
    ) -> BaseMessage:
        generation = self.generate(
            [messages], stop=stop, callbacks=callbacks, **kwargs
        ).generations[0][0]
        
        
        if isinstance(generation, ChatGeneration):
            return generation.message
        else:
            if hasattr(generation, 'text'):
                return AIMessage(content=generation.text)
            else:
                raise ValueError("Unexpected generation type")
    
    def invoke(
        self,
        input: LanguageModelInput,
        config: Optional[RunnableConfig] = None,
        *,
        stop: Optional[List[str]] = None,
        **kwargs: Any,
    ) -> BaseMessageChunk:
        config = config or {}
        return cast(
            BaseMessageChunk,
            cast(
                Generation,
                self.generate_prompt(
                    [self._convert_input(input)],
                    stop=stop,
                    callbacks=config.get("callbacks"),
                    tags=config.get("tags"),
                    metadata=config.get("metadata"),
                    **kwargs,
                ).generations[0][0],
            ).text,
        )

    # Propiedad para obtener los parámetros predeterminados
    @property
    def _default_params(self) -> Dict[str, Any]:
        """Obtener los parámetros predeterminados para llamar a la API de Llamus."""
        return {
            "model": self.model_name,
            "request_timeout": self.request_timeout,
            "max_tokens": self.max_tokens,
            "stream": self.streaming,
            "temperature": self.temperature,
            **self.model_kwargs,
        }
    
    @property
    def _identifying_params(self) -> Dict[str, Any]:
        """Get the identifying parameters."""
        return {**{"model_name": self.model_name, "base_path": self.base_path}, **self._default_params}

    @property
    def lc_secrets(self) -> Dict[str, str]:
        return {"api_key": "API_KEY"}

    @property
    def _llm_type(self) -> str:
        return "llamus-chat"
    
# from langchain_core.language_models.base import BaseLanguageModel
# from langchain_core.outputs import LLMResult
# from langchain_core.prompt_values import PromptValue
# from langchain_core.utils import get_pydantic_field_names
# class LlamusBLM(
#     BaseLanguageModel
# ):
#     """Abstract base class for interfacing with language models.

#     All language model wrappers inherit from BaseLanguageModel.
#     """

#     api_key: str = None
#     base_path: str = None
#     temperature: float = 0.7
#     model_name:str = Field(default="llama2:13b-chat", alias="model")
#     """Nombre del modelo a utilizar."""

    
#     @abstractmethod
#     def generate_prompt(
#         self,
#         prompts: List[PromptValue],
#         stop: Optional[List[str]] = None,
#         callbacks: Callbacks = None,
#         **kwargs: Any,
#     ) -> LLMResult:
#         """Pass a sequence of prompts to the model and return model generations.

#         This method should make use of batched calls for models that expose a batched
#         API.

#         Use this method when you want to:
#             1. take advantage of batched calls,
#             2. need more output from the model than just the top generated value,
#             3. are building chains that are agnostic to the underlying language model
#                 type (e.g., pure text completion models vs chat models).

#         Args:
#             prompts: List of PromptValues. A PromptValue is an object that can be
#                 converted to match the format of any language model (string for pure
#                 text generation models and BaseMessages for chat models).
#             stop: Stop words to use when generating. Model output is cut off at the
#                 first occurrence of any of these substrings.
#             callbacks: Callbacks to pass through. Used for executing additional
#                 functionality, such as logging or streaming, throughout generation.
#             **kwargs: Arbitrary additional keyword arguments. These are usually passed
#                 to the model provider API call.

#         Returns:
#             An LLMResult, which contains a list of candidate Generations for each input
#                 prompt and additional model provider-specific output.
#         """

#         # Call each callback function passing relevant arguments
#         for callback in self.callbacks:
#             callback(prompts, stop, **kwargs)

#         llm = Llamus(
#             n=self.max_retries,
#             api_key=self.api_key,
#             endpoint=self.base_path,
#             model=self.model_name
#         )
#         messages = [BaseMessage(content=prompt.content) for prompt in prompts]
#         prompt = "\n".join([f"Role: {message.role}\nContent:\n{message.content}" for message in prompts])
#         return llm._call(prompt, stop=stop, **kwargs)

#     @abstractmethod
#     async def agenerate_prompt(
#         self,
#         prompts: List[PromptValue],
#         stop: Optional[List[str]] = None,
#         callbacks: Callbacks = None,
#         **kwargs: Any,
#     ) -> LLMResult:
#         """Asynchronously pass a sequence of prompts and return model generations.

#         This method should make use of batched calls for models that expose a batched
#         API.

#         Use this method when you want to:
#             1. take advantage of batched calls,
#             2. need more output from the model than just the top generated value,
#             3. are building chains that are agnostic to the underlying language model
#                 type (e.g., pure text completion models vs chat models).

#         Args:
#             prompts: List of PromptValues. A PromptValue is an object that can be
#                 converted to match the format of any language model (string for pure
#                 text generation models and BaseMessages for chat models).
#             stop: Stop words to use when generating. Model output is cut off at the
#                 first occurrence of any of these substrings.
#             callbacks: Callbacks to pass through. Used for executing additional
#                 functionality, such as logging or streaming, throughout generation.
#             **kwargs: Arbitrary additional keyword arguments. These are usually passed
#                 to the model provider API call.

#         Returns:
#             An LLMResult, which contains a list of candidate Generations for each input
#                 prompt and additional model provider-specific output.
#         """


    
#     @deprecated("0.1.7", alternative="invoke", removal="0.2.0")
#     @abstractmethod
#     def predict(
#         self, text: str, *, stop: Optional[Sequence[str]] = None, **kwargs: Any
#     ) -> str:
#         """Pass a single string input to the model and return a string.

#          Use this method when passing in raw text. If you want to pass in specific
#             types of chat messages, use predict_messages.

#         Args:
#             text: String input to pass to the model.
#             stop: Stop words to use when generating. Model output is cut off at the
#                 first occurrence of any of these substrings.
#             **kwargs: Arbitrary additional keyword arguments. These are usually passed
#                 to the model provider API call.

#         Returns:
#             Top model prediction as a string.
#         """


#     @deprecated("0.1.7", alternative="invoke", removal="0.2.0")
#     @abstractmethod
#     def predict_messages(
#         self,
#         messages: List[BaseMessage],
#         *,
#         stop: Optional[Sequence[str]] = None,
#         **kwargs: Any,
#     ) -> BaseMessage:
#         """Pass a message sequence to the model and return a message.

#         Use this method when passing in chat messages. If you want to pass in raw text,
#             use predict.

#         Args:
#             messages: A sequence of chat messages corresponding to a single model input.
#             stop: Stop words to use when generating. Model output is cut off at the
#                 first occurrence of any of these substrings.
#             **kwargs: Arbitrary additional keyword arguments. These are usually passed
#                 to the model provider API call.

#         Returns:
#             Top model prediction as a message.
#         """


#     @deprecated("0.1.7", alternative="ainvoke", removal="0.2.0")
#     @abstractmethod
#     async def apredict(
#         self, text: str, *, stop: Optional[Sequence[str]] = None, **kwargs: Any
#     ) -> str:
#         """Asynchronously pass a string to the model and return a string.

#         Use this method when calling pure text generation models and only the top
#             candidate generation is needed.

#         Args:
#             text: String input to pass to the model.
#             stop: Stop words to use when generating. Model output is cut off at the
#                 first occurrence of any of these substrings.
#             **kwargs: Arbitrary additional keyword arguments. These are usually passed
#                 to the model provider API call.

#         Returns:
#             Top model prediction as a string.
#         """


#     @deprecated("0.1.7", alternative="ainvoke", removal="0.2.0")
#     @abstractmethod
#     async def apredict_messages(
#         self,
#         messages: List[BaseMessage],
#         *,
#         stop: Optional[Sequence[str]] = None,
#         **kwargs: Any,
#     ) -> BaseMessage:
#         """Asynchronously pass messages to the model and return a message.

#         Use this method when calling chat models and only the top
#             candidate generation is needed.

#         Args:
#             messages: A sequence of chat messages corresponding to a single model input.
#             stop: Stop words to use when generating. Model output is cut off at the
#                 first occurrence of any of these substrings.
#             **kwargs: Arbitrary additional keyword arguments. These are usually passed
#                 to the model provider API call.

#         Returns:
#             Top model prediction as a message.
#         """


#     @property
#     def _identifying_params(self) -> Mapping[str, Any]:
#         """Get the identifying parameters."""
#         return {}

#     def get_token_ids(self, text: str) -> List[int]:
#         """Return the ordered ids of the tokens in a text.

#         Args:
#             text: The string input to tokenize.

#         Returns:
#             A list of ids corresponding to the tokens in the text, in order they occur
#                 in the text.
#         """
#         return _get_token_ids_default_method(text)

#     def get_num_tokens(self, text: str) -> int:
#         """Get the number of tokens present in the text.

#         Useful for checking if an input will fit in a model's context window.

#         Args:
#             text: The string input to tokenize.

#         Returns:
#             The integer number of tokens in the text.
#         """
#         return len(self.get_token_ids(text))


#     def get_num_tokens_from_messages(self, messages: List[BaseMessage]) -> int:
#         """Get the number of tokens in the messages.

#         Useful for checking if an input will fit in a model's context window.

#         Args:
#             messages: The message inputs to tokenize.

#         Returns:
#             The sum of the number of tokens across the messages.
#         """
#         return sum([self.get_num_tokens(get_buffer_string([m])) for m in messages])


#     @classmethod
#     def _all_required_field_names(cls) -> Set:
#         """DEPRECATED: Kept for backwards compatibility.

#         Use get_pydantic_field_names.
#         """
#         return get_pydantic_field_names(cls)

# if __name__ == "__main__":
#     llm = Llamus(
#         n=10,
#         model="Null",
#         api_key=API_KEY,
#         endpoint=LLAMUS_BASE_PATH)
#     models = llm.list_models()
#     salto_linea = "\n- "
#     print(f"Modelos disponibles: {salto_linea}{salto_linea.join(models)}")
#     model = LlamusChat(
#         model_name=models[0],
#         api_key=API_KEY,
#         base_path=LLAMUS_BASE_PATH)
#     response = model.invoke([HumanMessage(content="Input: Funciona bien langchain con la api de ollama?\nOutput:")])
#     print(f"Respuesta LLM: {response}")
    
    